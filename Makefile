# Makefile for GNU/ident
#

COMPONENT ?= ident
INSTAPP    = ${INSTDIR}${SEP}!GNUIdent
OBJS       = ident rcsmap
CDEFINES   = -DRCS_lint=0
CINCLUDES  = -IC:
INSTAPP_FILES = !Boot !Help !Run !Setup !Sprites [!Sprites22] Messages Templates ident
INSTAPP_VERSION = Desc

include CApp

# Dynamic dependencies:
